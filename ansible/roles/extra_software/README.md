# extra_software

- [extra_software](#extra_software)
  - [Acciones disponibles](#acciones-disponibles)
  - [Documentación](#documentación)
    - [Ether Cloud Service client](#ether-cloud-service-client)

## Acciones disponibles

| **TAG** | **DESCRIPCIÓN** |
|---------|-----------------|
| **extra_software** | Ejecuta el role completo |
| **editors** | Instala los editores de texto `Visual Studio Code`, `Atom`, `Sublime Text` y `PyCharm` |
| **pass_managers** | Instala `KeepassXC` y `Buttercup` |
| **remotes** | Instala `Teamviewer` y `No Machine` |
| **socials** | Instala los clientes de mensajería `Telegram Desktop`, `Slack` y `Skype` |
| **windscribe** | Instala los clientes de VPN `Forti Client VPN` y `Windscribe` |
| **docker** | Instala `Docker Community Edition` |
| **dropbox** | Instala `Dropbox` |
| **code** | Instala `Visual Studio Code` |
| **spotify** | Instala `Spotify` |
| **spotify_cli** | Instala cliente `Spotify TUI` |
| **ohmyzsh** | Instala `Oh my ZSH!` |
| **sublime** | Instala `Sublime Text` |
| **atom** | Instala `Atom` |
| **gitkraken** | Instala `Gitkraken` |
| **telegram** | Instala `Telegram Desktop` |
| **slack** | Instala `Slack` |
| **forticlientvpn** | Instala `Forti Client VPN` |
| **tlp** | Instala `TLP` |
| **virtualbox** | Instala `Virtualbox` |
| **pycharm** | Instala `PyCharm` |
| **keepass** | Instala `KeepassXD` |
| **buttercap** | Instala `Buttercup` |
| **windscribe** | Instala `Windscribe Client VPN` |
| **chrome** | Instala `Google Chrome` |
| **chromium** | Instala `Chromium` |
| **teamviewer** | Instala `Teamviewer` |
| **nomachine** | Instala `No Machine` |
| **ecs** | Instala `Ether Cloud Services Client` |
| **postman** | Instala `Postman` |
| **skype** | Instala `Skype` |
| **java** | Instala `Java OpenJDK` |
| **apachedirectory** | Instala `Apache Directory Studio` |
| **rambox** | Instala `Rambox` |
| **franz** | Instala `Franz` |
| **deluge** | Instala `Deluge` |
| **cherrytree** | Instala `Cherry Tree` |
| **kubectl** | Instala `Kubectl` |
| **megacmd** | Instala `MEGAcmd` |
| **megasync** | Instala `MEGA sync` |
| **vivaldi** | Instala `Vivaldi` |
| **brave** | Instala `Brave` |
| **bitwarden** | Instala `Bitwarden` |
| **lexnet** | Instala `Lexnet` |
| **pass** | Instala gestor de secretos `Pass` |
| **bucklespring** | Instala `bucklespring` |
| **protonvpn** | Instala `ProtonVPN` |
| **prezto** | Instala `Prezto` |
| **etcher** | Instala `Balena Etcher` |
| **woeusb** | Instala `WoeUSB` |
| **go** | Instala `GO` |
| **fzf** | Instala `FZF` |
| **vim_plugins** | Instala plugins de `vim` |
| **flatpak** | |
| **vagrant** | |
| **dockercompose** | |
| **helm** | |
| **hetznerkube** | |

## Documentación

### Ether Cloud Service client

Para configurar el ecs-cli es necesario seguir [estos pasos](https://platform.bbva.com/en-us/developers/ether-cli/documentation/03-getting-started)
